define(function(require) {

  /**
   * What does this snaplet accomplish?
   *  - Visualization of data by using VMs to loop over properties, output simple values, ...
   */

  var $ = require('jQuery'),

    /**
     * Gumbuya.Snaplet.Templated is a basic form of snaplet that expects to output HTML.
     * If you are building a snaplet that does not output any HTML, you can extend from Gumbuya.Snaplet
     *
     * Read:
     *  - Basic foundations of snaplets and how they work: Gumbuya.Snaplet.js
     *  - Minimal snaplet with view that outputs HTML: Gumbuya.Snaplet.Templated.js
     */
    BaseSnaplet = require('gs!Gumbuya.Snaplet.Templated'),

    /**
     * Gumbuya.View.Templated is, just like its Snaplet counterpart, a basic form of View.
     * Read: Same as Snaplet, replace "Snaplet" with "View".
     *
     *  NOTE: Gumbuya.View.Templated carries a reference to our templating engine, which is in dire need of
     *    extension.
     */
    BaseView = require('gs!Gumbuya.View.Templated'),

    /**
     * ViewModel. Read up on this file to see how our ViewModels work.
     *
     * ViewModels in general are responsible for mapping data to HTML. They serve as the bridge between the
     * raw data-models and HTML.
     */
    ViewModel = require('gs!Gumbuya.ViewModel');

  require('gs!./style.less');

  var Snaplet = BaseSnaplet.extend({

    getViewModel: function(context) {

      var vm = new ViewModel({
        // Simple string values will live update as you change them, just like any other VM property
        simpleString: "ONE",

        dependsOnSimpleString: {
          get: function() {
            if (this.simpleString === 'ONE') {
              return "He was onee!!!!";
            } else {
              return "He was something else :( xoxox";
            }
          },
          depends: ['simpleString']
        },

        // You can proxy ticket elements
        //
        // 2-way state syncrhonization between ticket (or other observable property) and viewmodel property
        // in the example below, the property 'title' will reflect the current value of context.ticket's
        // "Title.Value" element, and live update as context.ticket changes. It will also propagate changes
        // from our vm.title property back to context.ticket
        title: ViewModel.proxy(
          // "context" contains 'ticket', 'episode', 'zone', 'mylife'
          // below we're proxying a value from context.ticket, which is the ticket that was passed in
          // when this snaplet was created
          context.ticket,

          // The element whose state we want to monitor
          'Title.Value'
        ),

        // Create a list. The list will also be live updating as I add/remove/change properties of the list
        simpleList: [],

        // Monitors all the binds under context.ticket. Whenver a bind is added/removed, 'ticketList' will
        // be updated as well.
        ticketList: ViewModel.proxyArray(context.ticket.binds, function(myBind) {
          return new ViewModel({
            shouldBeIncluded: myBind.isInside,
            name: ViewModel.proxy(myBind.to, 'FirstName.Value'),
            photo: ViewModel.proxy(myBind.to, 'Photo.OriginalUrl')
          });
        }),

        filteredTicketList: {
          get: function() {
            return this.ticketList.filter(function(vm) {
              return vm.shouldBeIncluded;
            });
          },
          depends: ['ticketList']
        }
      });

      // simpleList will now have 3 items, each with a property 'prop' valued 1, 2, 3 in that order
      // this, as you can see, is a manually populated list, unlike 'ticketList which will be automatically updated
      vm.simpleList.push(new ViewModel({prop: '1'}));
      vm.simpleList.push(new ViewModel({prop: '2'}));
      vm.simpleList.push(new ViewModel({prop: '3'}));

      // Example live updating property
      setTimeout(function() {
        // executes after 1s
        vm.simpleString = 'TWO';
      }, 1000);
      return vm;
    },

    ViewConstructor: BaseView.extend({
      htmlStringTemplate: require('gs!./View.html')
    })

  });

  return Snaplet;
});
